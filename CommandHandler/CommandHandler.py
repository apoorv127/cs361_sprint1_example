# Import all the Models

# Create your Class "CommandHandler"
# Add relevant methods that check the  user input, and that acts based on that (mainly you will need to parse the commands)

# Don't mess your views.py file. I had simple example to show, so I did that.

# Your logic for views.py will be like:
#
#  1. import the CommandHandler class
#  2. based on user input (parsing the command), the class will call that method
#  3. render the output to your index.html file


from scrum_manager_app.models import Employee

class CommandHandler:

    def scrum_manager_logged_in(self):
        filter_employee = Employee.objects.get(name="scrum_master", role="scrum_manager", isLoggedOn=True)
        if filter_employee.role == "scrum_manager" and filter_employee.isLoggedOn is True:
            return True
        else:
            return False


    def check_user_command(self, command_args):
        command_args_list = command_args.split(" ")
        first_check = command_args_list[0]
        second_check = command_args_list[1]


        message = 'Invalid Command'


        if first_check == "login" and second_check == "scrum_master":
            message = self.scrum_manager_logged_in()

        if first_check == "create" and second_check == "developer":
            message = self.create_developer_account(command_args_list)

        return message

    def check_duplicate_employee(self, command_args_list):
        employee = Employee.objects.filter(name=command_args_list[2])
        if employee:
            return f"Employee with username {command_args_list[2]} already exists"
        else:
            return f"No Employee with that username found"


    def create_developer_account(self, command_args_list):
        scrum_manager = self.scrum_manager_logged_in()

        if scrum_manager is False:
            return f"You are not the Scrum Manager!"

        message = self.check_duplicate_employee(command_args_list)

        if message == "No Employee with that username found":
            Employee.objects.create(name=command_args_list[2], email=command_args_list[3], role=command_args_list[1])
            return f"{command_args_list[1]} account for {command_args_list[2]} with email: {command_args_list[3]} created successfully"

        return message





