
from django.test import TestCase
from scrum_manager_app.models import Employee


class EmployeeRoleTestCase(TestCase):

    def setUp(self):
        self.employee_kate = Employee.objects.create(name="Kate", email="kate@uwm.edu", isLoggedOn=False)
        self.employee_qwerty = Employee.objects.create(name="qwerty", email="qwerty@uwm.edu", role="scrum_manager", isLoggedOn=False)
        self.employee_vikings = Employee.objects.create(name="vikings", email="vikings@uwm.edu", role="developer")
        self.employee_pilot =  Employee.objects.create(name="pilot", email="pilot@uwm.edu", role="intern")

    def test_role_not_scrum_manager(self):
        self.assertFalse(self.employee_kate.check_is_scrum_manager(self.employee_kate.name))
        self.assertFalse(self.employee_vikings.check_is_scrum_manager(self.employee_vikings.name))
        self.assertFalse(self.employee_pilot.check_is_scrum_manager(self.employee_pilot.name))

    def test_role_yes_scrum_manager(self):
        self.assertTrue(self.employee_qwerty.check_is_scrum_manager(self.employee_qwerty.name))

    def test_role_yes_developer(self):
        self.assertTrue(self.employee_vikings.check_is_developer(self.employee_vikings.name))

    def test_role_no_developer(self):
        self.assertFalse(self.employee_kate.check_is_developer(self.employee_kate.name))
        self.assertFalse(self.employee_qwerty.check_is_developer(self.employee_qwerty.name))
        self.assertFalse(self.employee_pilot.check_is_developer(self.employee_pilot.name))

    def test_role_yes_intern(self):
        self.assertTrue(self.employee_pilot.check_is_intern(self.employee_pilot.name))

    def test_role_no_intern(self):
        self.assertFalse(self.employee_kate.check_is_intern(self.employee_kate.name))
        self.assertFalse(self.employee_qwerty.check_is_intern(self.employee_qwerty.name))
        self.assertFalse(self.employee_vikings.check_is_intern(self.employee_vikings.name))



    # def tearDown(self):
    #     self.employee_kate.delete()
    #     self.employee_qwerty.delete()
    #     self.employee_vikings.delete()
    #     self.employee_pilot.delete()


# suite = unittest.TestSuite()
# suite.addTest(unittest.makeSuite(EmployeeRoleTestCase))
# runner = unittest.TextTestRunner()
# res = runner.run(suite)
# print(res)
# print("*" * 20)
# for i in res.failures:
#     print(i[1])
