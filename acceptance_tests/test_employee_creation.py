from django.test import TestCase
from scrum_manager_app.models import Employee
from CommandHandler.CommandHandler import CommandHandler

class EmployeeCreationAcceptanceTest(TestCase):
    def setUp(self):

        self.employee_qwerty = Employee.objects.create(name="scrum_master", email="qwerty@uwm.edu", role="scrum_manager", isLoggedOn=True)
        self.employee_spiderman = Employee.objects.create(name="spiderman", email="spiderman@uwm.edu", role="developer")
        self.app = CommandHandler()

    def test_scrum_manager_create_duplicate_developer_account(self):
        self.assertTrue(self.app.check_user_command("login scrum_master"))
        self.assertEqual(self.app.check_user_command("create developer spiderman spiderman@uwm.edu"), "Employee with username spiderman already exists")

    def test_scrum_manager_create_new_developer_account(self):
        self.assertTrue(self.app.check_user_command("login scrum_master"))
        self.assertEqual(self.app.check_user_command("create developer batman batman@uwm.edu"),
                         "developer account for batman with email: batman@uwm.edu created successfully")


    # def tearDown(self):
        # Employee.objects.all().delete()


# suite = unittest.TestSuite()
# suite.addTest(unittest.makeSuite(EmployeeCreationAcceptanceTest))
# runner = unittest.TextTestRunner()
# res = runner.run(suite)
# print(res)
# print("*" * 20)
# for i in res.failures:
#     print(i[1])
