from django.shortcuts import render
from django.views import View
from django.shortcuts import get_object_or_404
from django.http import Http404
from scrum_manager_app.models import Employee, Project, UserStories
from CommandHandler.CommandHandler import CommandHandler

# Create your views here.
class Home(View):
    def get(self, request):
        return render(request, 'main/index.html')

    def post(self, request):
        user_input = request.POST["command"]
        command_input_handler = CommandHandler()
        if user_input:
            message = command_input_handler.check_user_command(user_input)
        else:
            message = "No Command given by the user yet"

        return render(request, 'main/index.html', {"message": message})
        # try:
        #     employee_scrum = get_object_or_404(Employee, name="scrum_master", role="scrum_manager", isLoggedOn=True)
        #     message = employee_scrum.check_is_duplicate(user_input)
        #     if message == "No Employee with that username found":
        #         message = employee_scrum.create_employee(user_input)
        #         return render(request, 'main/index.html', {"message": message})
        #     else:
        #         return render(request, 'main/index.html', {"message": message})
        # except Employee.DoesNotExist:
        #     return render(request, 'main/index.html', {"message": "you are not the scrum manager"})





