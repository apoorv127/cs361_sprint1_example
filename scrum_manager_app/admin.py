from django.contrib import admin

# Register your models here.
from .models import Employee, Project, UserStories

admin.site.register(Employee)
admin.site.register(Project)
admin.site.register(UserStories)