from django.apps import AppConfig


class ScrumManagerAppConfig(AppConfig):
    name = 'scrum_manager_app'
