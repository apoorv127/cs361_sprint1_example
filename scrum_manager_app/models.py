from django.db import models

# Create your models here.
class Employee(models.Model):
    name       =  models.CharField(max_length=200)
    email      =  models.CharField(max_length=100)
    password   =  models.CharField(max_length=20, default="password")
    phone      =  models.CharField(max_length=10, default="0000000000")
    role       =  models.CharField(max_length=20, default="employee")
    isLoggedOn =  models.BooleanField(default=False)


   # Methods for Unit Tests
    def check_is_scrum_manager(self, name):
        self.employee = Employee.objects.get(name=name)
        return self.employee.role == "scrum_manager"

    def check_is_developer(self, name):
        self.employee = Employee.objects.get(name=name)
        return self.employee.role == "developer"

    def check_is_intern(self, name):
        self.employee = Employee.objects.get(name=name)
        return self.employee.role == "intern"

    # def check_is_duplicate(self, role_params):
    #     role_params = role_params.split(" ")
    #     self.employee = Employee.objects.filter(name=role_params[2])
    #     if self.employee:
    #         return f"Employee with username {role_params[2]} already exists"
    #     else:
    #         return f"No Employee with that username found"
    #
    #
    # def create_employee(self, role_params):
    #     role_params = role_params.split(" ")
    #     if role_params[1] == 'developer':
    #         Employee.objects.create(name=role_params[2], email=role_params[3], role=role_params[1])
    #         return f"{role_params[1]} account for {role_params[2]} with email: {role_params[3]} created successfully"
    #     else:
    #         return f"Failure in {role_params[1]} account for {role_params[2]} with email: {role_params[3]}. Hence, not created!"

    def __str__(self):
        return self.name

class Project(models.Model):
    name             = models.CharField(max_length=200)
    project_employee = models.OneToOneField(Employee, on_delete=models.CASCADE)

    # def check__duplicate_project(self, command_params):
    #     command_params = command_params.split(" ")
    #     if command_params[1] == 'project':
    #         self.project = Project.objects.filter(name=command_params[2])
    #     if self.project:
    #         return f"Project with name {command_params[2]} already exists"


    def __str__(self):
        return self.name

class UserStories(models.Model):
    description          = models.TextField(max_length=500)
    user_stories_project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

